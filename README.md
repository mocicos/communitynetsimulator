# CommunityNetSimulator

This agent-based model simulates the global behaviour of a Community of Practice.

## Overview

This model simulates the global behavior of a Community of Practice (CoP), a
group of people who share a concern, set of problems, or passion for a given
topic, and who deepen their knowledge and expertise in that area through ongoing
interacting on an ongoing basis. In this perspective, users ask questions and
others answer. This simulation aims at understanding how users interact with
each other.

## Requirements

CommunityNetSimulator is built upon
[NetLogo](https://ccl.northwestern.edu/netlogo/), a programming language and an
IDE for agent-based modeling.

## Reference

Community Net Simulator: Using simulations to study online community networks.
Zhang, Jun, Ackerman, Mark S and Adamic, Lada. International Conference on
Communities and Technologies 2007, 295–321, 2007, Springer.

## Contributors

Copyright (C) Amal Chaoui, Jarod Vanderlynden and Maxime Morge, 2022

